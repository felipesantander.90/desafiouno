"""Desafio_Uno URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from nivel1 import views
from nivel2.views import getFechas,view_nivel2
from nivel3.views import view_nivel3

urlpatterns = [
    path('admin/', admin.site.urls),
    path('/', views.home, name='home'),
    path('nivel2/', view_nivel2, name='home1'),
    path('nivel1/', views.model_form_upload, name='model_form_upload'),
    path('nivel2/get_fechas/',getFechas),
    path('nivel3/',view_nivel3),
]
