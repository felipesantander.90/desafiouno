Hola, Muchas gracias, por darme la oportunidad de participar en su seleccion,
Realice este test con django , docker y docker-compose por lo que se debe tener instalado docker en la maquina o en su defecto python3
con django
Ademas deje todo el proyecto en mi servidor:

nivel 1: http://167.99.123.59:8001/nivel1/
nivel 2: http://167.99.123.59:8001/nivel2/
nivel 3: http://167.99.123.59:8001/nivel3/

por si ocurre cualquier inconveniente al levantar el proyecto.
para poder ejecutar el proyecto con docker se debe

1. ingresar a la carpeta del proyecto, una vez ahi comprovar si la carpeta contiene el archivo docker-compose
2. ejecutar con docker-docker compose : docker-compose up --build, esperar a que se resuelvan las configuracion y dirigirse a su navegador
abrir http://localhost:8001/

para abrir el proyecto sin docker:
instalar pyrhon3
ejecutar :
1.  pip3 install --upgrade pip -r requirements.txt
2.  python3 manage.py makemigrations
3.  python3 manage.py migrate
4.  python3 manage.py runserver 0.0.0.0:8001 y dirigirse a su navegador
abrir http://localhost:8001/

Saludos y gracias