from django.test import TestCase
import json


def meses_entre_fechas(startDate= '2016-1-28',endDate= '2017-3-26'):
    from datetime import datetime
    from dateutil.relativedelta import relativedelta

    cur_date = start = datetime.strptime(startDate, '%Y-%m-%d').date()
    end = datetime.strptime(endDate, '%Y-%m-%d').date()

    meses=[]

    while cur_date < end:
        meses.append(str(cur_date))
        cur_date += relativedelta(months=1)
    return meses
string_json= '''{
    "id": 6,
    "fechaCreacion": "1969-03-01",
    "fechaFin": "1970-01-01",
    "fechas": [
      "1969-03-01",
      "1969-05-01",
      "1969-09-01",
      "1970-01-01"]
}'''
dates_json = json.loads(string_json)
fechaCreacion=dates_json['fechaCreacion']
fechaFin=dates_json['fechaFin']
fechas=dates_json['fechas']
print ('gato' in dates_json)
print ('fechaFin' in dates_json)
r_fecha=meses_entre_fechas(fechaCreacion,fechaFin)
print (r_fecha)
print (set(fechas))
print ("************")
print(set(r_fecha).difference(set(fechas)))