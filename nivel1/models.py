from django.db import models

class Document(models.Model):
    document = models.FileField(upload_to='documents/')
    uploaded_at = models.DateTimeField(auto_now_add=True)

class Salida(models.Model):
    id          = models.AutoField(primary_key=True)
    entrada     = models.TextField()
    respuesta   = models.TextField()