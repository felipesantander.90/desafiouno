  
from django.shortcuts import render, redirect
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from nivel1.models import Document,Salida
from nivel1.forms import DocumentForm
import json
import copy

def home(request):
    ultima_respuesta = Salida.objects.last()
    return render(request, 'home.html', { 'salida': ultima_respuesta })

def model_form_upload(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            #form.save()
            for filename, file in request.FILES.items():
                file_content= request.FILES[filename].read()
                respuesta,respuesta_content  = validar_archivo(file_content)
                print (respuesta,respuesta_content)
                respuesta_final=Salida(entrada=respuesta_content,respuesta=respuesta)
                respuesta_final.save()
            return redirect('home')
    else:
        form = DocumentForm()
    return render(request, 'model_form_upload.html', {
        'form': form
    })

def validar_archivo(content_file):

    try:
        content_json=json.loads(content_file)
        if 'fechaCreacion' in content_json and 'fechaFin' in content_json and 'fechas' in content_json:
            fechaCreacion=content_json['fechaCreacion']
            fechaFin=content_json['fechaFin']
            fechas=content_json['fechas']
            r_fecha=meses_entre_fechas(fechaCreacion,fechaFin)
            print (r_fecha)
            r_meses_faltantes=set(r_fecha).difference(set(fechas))
            new_json= copy.deepcopy(content_json)
            del new_json['fechas']
            new_json['fechasFaltantes']= r_meses_faltantes
            return new_json, content_json
        else :
            return 'faltan parametros', content_json
    except ValueError as e:
        return "No es un archivo python valido", ''

def meses_entre_fechas(startDate= '2016-1-28',endDate= '2017-3-26'):
    from datetime import datetime
    from dateutil.relativedelta import relativedelta

    cur_date = start = datetime.strptime(startDate, '%Y-%m-%d').date()
    end = datetime.strptime(endDate, '%Y-%m-%d').date()
    meses=[]
    while cur_date < end:
        meses.append(str(cur_date))
        cur_date += relativedelta(months=1)
    return meses