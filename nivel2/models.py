from django.db import models

# Create your models here.
from django.db import models

class FechasModelo(models.Model):
    fechaCreacion = models.CharField(max_length=200)
    fechaFin = models.CharField(max_length=200)

class Fechas(models.Model):
    fecha_modelo = models.ForeignKey(FechasModelo, related_name='FechasModelo', on_delete=models.CASCADE)
    fecha = models.CharField(max_length=200)
