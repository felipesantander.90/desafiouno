from django.shortcuts import render
from django.http import Http404
import json
from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.http import JsonResponse
from django.core import serializers
from django.conf import settings
import json
import requests
import copy

@api_view(['GET', 'POST'])
def getFechas(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        return JsonResponse({
    "id": 6,
    "fechaCreacion": "1969-03-01",
    "fechaFin": "1970-01-01",
    "fechas": [
      "1969-03-01",
      "1969-05-01",
      "1969-09-01",
      "1970-01-01"]
})

    elif request.method == 'POST':
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

def view_nivel2(request):
    response = requests.get('http://167.99.123.59:8001/nivel2/get_fechas/')
    print (response.json())
    res = validar_archivo(response.json())
    return render(request, 'home1.html', { 'salida':  res})


def validar_archivo(content_json):
    try:
        if 'fechaCreacion' in content_json and 'fechaFin' in content_json and 'fechas' in content_json:
            fechaCreacion=content_json['fechaCreacion']
            fechaFin=content_json['fechaFin']
            fechas=content_json['fechas']
            r_fecha=meses_entre_fechas(fechaCreacion,fechaFin)
            print (r_fecha)
            r_meses_faltantes=set(r_fecha).difference(set(fechas))
            new_json= copy.deepcopy(content_json)
            new_json['fechasFaltantes']= r_meses_faltantes
            return new_json
        else :
            return 'faltan parametros'
    except ValueError as e:
        return "No es un archivo python valido"

def meses_entre_fechas(startDate= '2016-1-28',endDate= '2017-3-26'):
    from datetime import datetime
    from dateutil.relativedelta import relativedelta

    cur_date = start = datetime.strptime(startDate, '%Y-%m-%d').date()
    end = datetime.strptime(endDate, '%Y-%m-%d').date()
    meses=[]
    while cur_date < end:
        meses.append(str(cur_date))
        cur_date += relativedelta(months=1)
    return meses