from django.shortcuts import render
from django.http import Http404
import json
from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.http import JsonResponse
from django.core import serializers
from django.conf import settings
import json
import requests
import copy
import numpy as np

@api_view(['GET', 'POST'])
def view_nivel3(request):
    if request.method == 'GET':
        r=getfechas()
        print('*******************')
        print(r)
        print('*******************')
        return HttpResponse(json.dumps(r), content_type="application/json")
    elif request.method == 'POST':
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

def getfechas():
    response = requests.get('http://167.99.123.59:8001/nivel2/get_fechas/')
    print (response.json())
    res = validar_archivo(response.json())
    return res


def validar_archivo(content_json):
    try:
        if 'fechaCreacion' in content_json and 'fechaFin' in content_json and 'fechas' in content_json:
            fechaCreacion=content_json['fechaCreacion']
            fechaFin=content_json['fechaFin']
            fechas=content_json['fechas']
            r_fecha=meses_entre_fechas(fechaCreacion,fechaFin)
            print (r_fecha)
            r_meses_faltantes=set(r_fecha).difference(set(fechas))
            new_json= copy.deepcopy(content_json)
            aux =[]
            for x in  r_meses_faltantes:
                aux.append(x)
            new_json['fechasFaltantes']= aux
            return new_json
        else :
            return 'faltan parametros'
    except ValueError as e:
        return "No es un archivo python valido"

def meses_entre_fechas(startDate= '2016-1-28',endDate= '2017-3-26'):
    from datetime import datetime
    from dateutil.relativedelta import relativedelta

    cur_date = start = datetime.strptime(startDate, '%Y-%m-%d').date()
    end = datetime.strptime(endDate, '%Y-%m-%d').date()
    meses=[]
    while cur_date < end:
        meses.append(str(cur_date))
        cur_date += relativedelta(months=1)
    return meses